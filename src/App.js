import React, { Component } from 'react';
import './App.css';
import Masthead from './components/Masthead'
import scriptParameters from "./scriptParameters.json";

class App extends Component {

  state = scriptParameters;

  render() {
    return (
      <div className="App">
        <Masthead
          fixed={this.state.fixed}
          width={this.state.width}
          div={this.state.div}
          ajaxReload={this.state.ajaxReload}
          rootRelative={this.state.rootRelative}
        />
      </div>
    );
  }
}

export default App;
