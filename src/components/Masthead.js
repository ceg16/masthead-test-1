import React from 'react';
import "./masthead_fixed.css";

function Masthead(props) {

  //test that props have been passed from ../App.js
  console.log(props);

  /* Additional variables that need to be calculated/defined
    styleSheet,
    widthType,
    headerOrDiv,
    urlPath
  */

  return (
    <div id="dul-masthead-dul-megamenu" style={{ maxWidth: "none", width: 990 }}>
      <header className="dul-masthead-no-touch">
        <div className="dul-masthead-region-logo">
          <a
            id="dul-logo-link"
            onclick="ga('send', 'event', { eventCategory: 'Header', eventAction: 'Branding', eventLabel: 'Library Logo'});"
            href="https://library.duke.edu/"
          >
            <img
              src="https://library.duke.edu/masthead/img/logo.png"
              alt="Duke University Libraries"
              id="dul-masthead-logo"
            />
          </a>
          <button
            id="dul-masthead-nav-trigger-btn"
            aria-haspopup="true"
            aria-expanded="false"
          >
            ☰ Menu
          </button>
          <ul
            hidden=""
            role="menu"
            className="dul-masthead-mobile-menu"
            id="dul-masthead-mobile-menu"
          >
            <li>
              <a role="menuitem" href="https://library.duke.edu/find">
                Search &amp; Find
              </a>
            </li>
            <li>
              <a role="menuitem" href="https://library.duke.edu/using">
                Using the Library
              </a>
            </li>
            <li>
              <a role="menuitem" href="https://library.duke.edu/research">
                Research Support
              </a>
            </li>
            <li>
              <a role="menuitem" href="https://library.duke.edu/course-support">
                Course Support
              </a>
            </li>
            <li>
              <a role="menuitem" href="https://library.duke.edu/libraries">
                Libraries
              </a>
            </li>
            <li>
              <a role="menuitem" href="https://library.duke.edu/about">
                About
              </a>
            </li>
          </ul>
        </div>
        <div className="dul-masthead-region-scoped-search">
          <form
            className="dul-masthead-mastSearch"
            action="//quicksearch.library.duke.edu/"
            onsubmit="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Top Links', eventLabel: 'Search Submit'});"
          >
            {/* Generic reusable hidden attributes */}
            <input
              type="hidden"
              name=""
              defaultValue=""
              id="dul-masthead-hiddenfilter1"
              className="dul-masthead-hiddenMastFilter"
            />
            <input
              type="hidden"
              name=""
              defaultValue=""
              id="dul-masthead-hiddenfilter2"
              className="dul-masthead-hiddenMastFilter"
            />
            <input
              type="hidden"
              name=""
              defaultValue=""
              id="dul-masthead-hiddenfilter3"
              className="dul-masthead-hiddenMastFilter"
            />
            <div>
              <input
                className="dul-masthead-mastSearchBox"
                placeholder="Search articles, books, our website & more"
                name="q"
                type="text"
                aria-label="Site Search"
              />
              <div className="dul-masthead-btn-group">
                <button
                  className="dul-masthead-btn"
                  aria-label="Search"
                  type="submit"
                  onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Top Links', eventLabel: 'Search Button'});"
                />
              </div>
            </div>
          </form>
        </div>
        <div className="dul-masthead-region-myaccount">
          <a
            id="dul-masthead-myAccounts"
            href="https://library.duke.edu/my-accounts"
            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Top Links', eventLabel: 'My Accounts'});"
          >
            My Accounts
          </a>
          <a
            href="https://library.duke.edu/research/ask"
            id="dul-masthead-ask-librarian-link"
            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Top Links', eventLabel: 'Ask a Librarian'});"
          >
            Ask a Librarian
          </a>
          <a
            id="dul-masthead-account-hours"
            href="https://library.duke.edu/about/hours"
            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Top Links', eventLabel: 'Library Hours'});"
          >
            Library Hours
          </a>
          <a
            id="dul-masthead-website-search-link"
            href="//quicksearch.library.duke.edu/searcher/website"
            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Top Links', eventLabel: 'Website Search'});"
          >
            Search our Site
          </a>
        </div>
        <div id="dul-masthead-region-megamenu">
          <nav
            role="navigation"
            aria-label="Main Duke University Libraries navigation"
          >
            <div className="dul-masthead-megamenu_container">
              {/* Begin Menu Container */}
              <ul className="dul-masthead-megamenu">
                {/* Begin Mega Menu */}
                <li
                  className="dul-masthead-megamenu_button"
                  style={{ display: "none" }}
                >
                  <a
                    className="dul-masthead-btn dul-masthead-dropdown-toggle"
                    href="#_"
                  >
                    Menu <span />
                  </a>
                </li>
                <li className="dul-masthead-mega_toplevel">
                  <a
                    href="https://library.duke.edu/find"
                    onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Search Find Portal'});"
                  >
                    Search &amp; Find
                  </a>
                  {/* Begin Item */}
                  <div
                    className="dul-masthead-dropdown_fullwidth"
                    style={{ maxWidth: "none", width: 958, left: 0, top: "auto" }}
                  >
                    {/* Begin Item Container */}
                    <div className="dul-masthead-menu-items">
                      <div className="dul-masthead-col_4">
                        <ul className="dul-masthead-primaryColumn">
                          <li>
                            <a
                              href="https://find.library.duke.edu/?utm_campaign=shared_mast&utm_content=mast_search_find_link&utm_source=shared_masthead&utm_medium=referral"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Books Media'});"
                            >
                              Books &amp; Media
                            </a>
                            <br />
                            <span className="dul-masthead-muted dul-masthead-smaller dul-masthead-normal">
                              Books, journals, films &amp; more
                            </span>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/find/articles"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Articles'});"
                            >
                              Articles
                            </a>
                            <br />
                            <span className="dul-masthead-muted dul-masthead-smaller dul-masthead-normal">
                              From journals, newspapers, magazines
                            </span>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/find/journal-titles"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Online Journal Titles'});"
                            >
                              Online Journal Titles
                            </a>
                            <br />
                            <span className="dul-masthead-muted dul-masthead-smaller dul-masthead-normal">
                              Find by title
                            </span>
                          </li>
                          <li>
                            <a
                              href="https://guides.library.duke.edu/az.php"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Research Databases'});"
                            >
                              Research Databases
                            </a>
                            <br />
                            <span className="dul-masthead-muted dul-masthead-smaller dul-masthead-normal">
                              Find by title and subject
                            </span>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_2">
                        <ul>
                          <li>
                            <a
                              href="https://library.duke.edu/find/film-video"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Film Video'});"
                            >
                              Film &amp; Video
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/music"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Music'});"
                            >
                              Music
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/data"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Data'});"
                            >
                              Data &amp; Digital Maps
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://guides.library.duke.edu/images"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Images'});"
                            >
                              Images
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/find/ebooks"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'eBooks'});"
                            >
                              eBooks
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/find/newspapers"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Newspapers'});"
                            >
                              Newspapers
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3">
                        <ul>
                          <li>
                            <a
                              href="https://library.duke.edu/rubenstein"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Special Collections'});"
                            >
                              Special Collections
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://repository.duke.edu/dc"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Digitized Collections'});"
                            >
                              Digitized Collections
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/find/scholarly"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Duke Faculty Scholarly Work'});"
                            >
                              Duke Faculty Scholarly Work
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/find/theses-dissertations"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Theses Dissertations'});"
                            >
                              Theses &amp; Dissertations
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://guides.library.duke.edu/primarysources"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Primary Sources'});"
                            >
                              Primary Sources
                            </a>
                          </li>
                          <li>
                            <a
                              href="//quicksearch.library.duke.edu/searcher/website"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Our Website'});"
                            >
                              Our Website
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3 dul-masthead-featureColumn">
                        <a
                          href="https://repository.duke.edu/dc"
                          onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Feature'});"
                          className="dul-masthead-featureLink"
                        >
                          <img
                            className="dul-masthead-megaFeature img-polaroid"
                            src="https://library.duke.edu/masthead/img/promos/find.jpg"
                            alt=""
                          />
                          <br />
                          <div className="dul-masthead-featureCaption">
                            Digitized Collections
                          </div>
                        </a>
                      </div>
                      <div className="dul-masthead-col_12 dul-masthead-auxLinks">
                        <div className="dul-masthead-col_2 dul-masthead-moreLink">
                          <a
                            href="https://library.duke.edu/find"
                            aria-label="Find more resources"
                            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'More'});"
                          >
                            More »
                          </a>
                        </div>
                        <div className="dul-masthead-col_10 dul-masthead-auxHelp">
                          Not sure where to start? See our{" "}
                          <a
                            href="http://guides.library.duke.edu"
                            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Find Menu', eventLabel: 'Aux Research Guides'});"
                          >
                            Research Guides
                          </a>{" "}
                          to explore by subject.
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* End Item Container */}
                </li>
                {/* End Item */}
                <li className="dul-masthead-mega_toplevel">
                  <a
                    href="https://library.duke.edu/using"
                    onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'Using the Library Portal'});"
                  >
                    Using the Library
                  </a>
                  {/* Begin Item */}
                  <div
                    className="dul-masthead-dropdown_fullwidth"
                    style={{ maxWidth: "none", width: 958, left: 0, top: "auto" }}
                  >
                    {/* Begin Item Container */}
                    <div className="dul-masthead-menu-items">
                      <div className="dul-masthead-col_3">
                        <ul>
                          <li>
                            <a
                              href="https://library.duke.edu/using/borrowing"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'Borrowing'});"
                            >
                              Borrowing
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/using/interlibrary-requests"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'ILL'});"
                            >
                              Interlibrary Requests
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/using/off-campus"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'Off Campus'});"
                            >
                              Connect from Off Campus
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/using/policies"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'Policies'});"
                            >
                              Library Policies
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3">
                        <ul>
                          <li>
                            <a
                              href="https://library.duke.edu/using/library-spaces/places-for-events"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'Reserve Meeting/Event Space'});"
                            >
                              Reserve Meeting/Event Space
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/using/library-spaces"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'Find Library Spaces'});"
                            >
                              Find Library Spaces
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/using/locationguide"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'Find Book Locations'});"
                            >
                              Find Book Locations
                            </a>
                          </li>
                          <li>
                            <a
                              href="http://twp.duke.edu/twp-writing-studio"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'Writing Studio'});"
                            >
                              Writing Studio
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3">
                        <ul>
                          <li>
                            <a
                              href="https://library.duke.edu/using/labs-software"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'Computer Lab Software'});"
                            >
                              Computer Labs &amp; Software
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/using/technology-lending"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'Tech Lending'});"
                            >
                              Technology Lending
                            </a>
                          </li>
                          <li>
                            <a
                              href="http://link.duke.edu/"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'The Link'});"
                            >
                              The Link
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3 dul-masthead-featureColumn">
                        <a
                          href="https://library.duke.edu/services/alumni"
                          className="dul-masthead-featureLink"
                          onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'Feature'});"
                        >
                          <img
                            className="dul-masthead-megaFeature img-polaroid"
                            src="https://library.duke.edu/masthead/img/promos/using-library-alumni.jpg"
                            alt=""
                          />
                          <br />
                          <div className="dul-masthead-featureCaption">
                            Library Services for Duke Alumni
                          </div>
                        </a>
                      </div>
                      <div className="dul-masthead-col_12 dul-masthead-auxLinks">
                        <div className="dul-masthead-col_2 dul-masthead-moreLink">
                          <a
                            href="https://library.duke.edu/using"
                            aria-label="More about using the libraries"
                            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'More'});"
                          >
                            More »
                          </a>
                        </div>
                        <div className="dul-masthead-col_10 dul-masthead-auxHelp">
                          Visiting the library? See our{" "}
                          <a
                            href="https://library.duke.edu/about/hours"
                            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'Aux Hours'});"
                          >
                            hours
                          </a>
                          and
                          <a
                            href="https://library.duke.edu/services/visitors"
                            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Using Menu', eventLabel: 'Aux Visitors'});"
                          >
                            services for visitors
                          </a>
                          .
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* End Item Container */}
                </li>
                {/* End Item */}
                <li className="dul-masthead-mega_toplevel">
                  <a
                    href="https://library.duke.edu/research"
                    onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'Research Support Portal'});"
                  >
                    Research Support
                  </a>
                  {/* Begin Item */}
                  <div
                    className="dul-masthead-dropdown_fullwidth"
                    style={{ maxWidth: "none", width: 958, left: 0, top: "auto" }}
                  >
                    {/* Begin Item Container */}
                    <div className="dul-masthead-menu-items">
                      <div className="dul-masthead-col_3">
                        <ul>
                          <li>
                            <a
                              href="https://directory.library.duke.edu/subject-specialists"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'Subject Specialists'});"
                            >
                              Subject Specialists
                            </a>
                          </li>
                          <li>
                            <a
                              href="http://guides.library.duke.edu"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'Research Guides'});"
                            >
                              Research Guides
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/about/depts/ias"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'International Research'});"
                            >
                              International Research
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/research/science"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'Science Research'});"
                            >
                              Science Research
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/research/humanities"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'Humanities Research'});"
                            >
                              Humanities Research
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3">
                        <ul>
                          <li>
                            <a
                              href="https://library.duke.edu/research/citing"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'Citing Sources'});"
                            >
                              Citing Sources
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/research/citing/tools"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'Citation Tools'});"
                            >
                              Citation Tools
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/research/copyright-advice"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'Copyright Advice'});"
                            >
                              Copyright in Research &amp; Teaching
                            </a>
                          </li>
                          <li>
                            <a
                              href="//scholarworks.duke.edu/publish-your-work-your-way/"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'Publish Archive'});"
                            >
                              Publish &amp; Archive Your Work
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3">
                        <ul>
                          <li>
                            <a
                              href="https://library.duke.edu/data/"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'Data Services'});"
                            >
                              Data Services
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/digital/consult"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'Digital Research Projects'});"
                            >
                              Digital Research Projects
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/edge"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'TheEdge'});"
                            >
                              The Edge
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3 dul-masthead-featureColumn">
                        <a
                          href="https://library.duke.edu/research/plagiarism"
                          className="dul-masthead-featureLink"
                          onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'Feature'});"
                        >
                          <img
                            className="dul-masthead-megaFeature img-polaroid"
                            src="https://library.duke.edu/masthead/img/promos/research-student.jpg"
                            alt=""
                          />
                          <br />
                          <div className="dul-masthead-featureCaption">
                            Avoiding Plagiarism
                          </div>
                        </a>
                      </div>
                      <div className="dul-masthead-col_12 dul-masthead-auxLinks">
                        <div className="dul-masthead-col_2 dul-masthead-moreLink">
                          <a
                            href="https://library.duke.edu/research"
                            aria-label="More about research support"
                            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'More'});"
                          >
                            More »
                          </a>
                        </div>
                        <div className="dul-masthead-col_10 dul-masthead-auxHelp">
                          Teaching or taking a Duke class? Explore our
                          <a
                            href="https://library.duke.edu/course-support"
                            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Research Menu', eventLabel: 'Aux Course Resources'});"
                          >
                            resources &amp; services for courses
                          </a>
                          .
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* End Item Container */}
                </li>
                {/* End Item */}
                <li className="dul-masthead-mega_toplevel">
                  <a
                    href="https://library.duke.edu/course-support"
                    onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Course Menu', eventLabel: 'Course Support Portal'});"
                  >
                    Course Support
                  </a>
                  {/* Begin Item */}
                  <div
                    className="dul-masthead-dropdown_fullwidth"
                    style={{ maxWidth: "none", width: 958, left: 0, top: "auto" }}
                  >
                    {/* Begin Item Container */}
                    <div className="dul-masthead-menu-items">
                      <div className="dul-masthead-col_4">
                        <ul>
                          <li>
                            <a
                              href="https://library.duke.edu/course-support/course-reserves"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Course Menu', eventLabel: 'Reserves'});"
                            >
                              Course Reserves
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/course-support/course-reserves/textbooks"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Course Menu', eventLabel: 'Top Textbooks'});"
                            >
                              Top Textbooks
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/course-support/guides"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Course Menu', eventLabel: 'Course Guides'});"
                            >
                              Course Guides
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_5">
                        <ul>
                          <li>
                            <a
                              href="https://library.duke.edu/course-support/training-workshops"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Course Menu', eventLabel: 'Training Workshops'});"
                            >
                              Training &amp; Workshops
                            </a>
                          </li>
                          <li>
                            <a
                              href="http://learninginnovation.duke.edu"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Course Menu', eventLabel: 'CIT'});"
                            >
                              Duke Learning Innovation
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://sakai.duke.edu/"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Course Menu', eventLabel: 'Sakai'});"
                            >
                              Login to Sakai
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3 dul-masthead-featureColumn">
                        <a
                          href="https://library.duke.edu/course-support/course-reserves/textbooks"
                          className="dul-masthead-featureLink"
                          onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Course Menu', eventLabel: 'Feature'});"
                        >
                          <img
                            className="dul-masthead-megaFeature img-polaroid"
                            src="https://library.duke.edu/masthead/img/promos/course-support-textbooks.jpg"
                            alt="Top Textbooks"
                          />
                          <br />
                          <div className="dul-masthead-featureCaption">
                            Top Textbooks
                          </div>
                        </a>
                      </div>
                      <div className="dul-masthead-col_12 dul-masthead-auxLinks">
                        <div className="dul-masthead-col_2 dul-masthead-moreLink">
                          <a
                            href="https://library.duke.edu/course-support"
                            aria-label="More about course support"
                            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Course Menu', eventLabel: 'More'});"
                          >
                            More »
                          </a>
                        </div>
                        <div className="dul-masthead-col_10 dul-masthead-auxHelp">
                          Need help? Meet a
                          <a
                            href="https://library.duke.edu/about/directory/subject-specialists/"
                            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Course Menu', eventLabel: 'Aux Subject Specialist'});"
                          >
                            subject specialist
                          </a>
                          , see our{" "}
                          <a
                            href="http://guides.library.duke.edu/"
                            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Course Menu', eventLabel: 'Aux Research Guides'});"
                          >
                            research guides
                          </a>
                          , or learn how to
                          <a
                            href="https://library.duke.edu/research/citing"
                            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Course Menu', eventLabel: 'Aux Cite Sources'});"
                          >
                            cite sources
                          </a>
                          .
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* End Item Container */}
                </li>
                {/* End Item */}
                <li className="dul-masthead-mega_toplevel">
                  <a
                    href="https://library.duke.edu/libraries"
                    onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'Libraries Portal'});"
                  >
                    Libraries
                  </a>
                  {/* Begin Item */}
                  <div
                    className="dul-masthead-dropdown_fullwidth"
                    style={{ maxWidth: "none", width: 958, left: 0, top: "auto" }}
                  >
                    {/* Begin Item Container */}
                    <div className="dul-masthead-menu-items">
                      <div className="dul-masthead-col_3">
                        <ul>
                          <li>
                            <a
                              href="/"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'Perkins'});"
                            >
                              Perkins &amp; Bostock Libraries
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/lilly"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'Lilly'});"
                            >
                              Lilly Library
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/music"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'Music'});"
                            >
                              Music Library
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/marine"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'Marine'});"
                            >
                              Marine Lab Library
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/lsc"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'LSC'});"
                            >
                              Library Service Center
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3">
                        <ul>
                          <li>
                            <a
                              href="https://library.duke.edu/rubenstein"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'Rubenstein'});"
                            >
                              Rubenstein Rare Book &amp; Manuscript Library
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/rubenstein/uarchives"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'UArchives'});"
                            >
                              Duke University Archives
                            </a>
                          </li>
                          <li>
                            <a
                              href="http://dku.edu.cn/en/academics/library"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'DKU'});"
                            >
                              Duke Kunshan University Library
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3">
                        <ul>
                          <li>
                            <a
                              href="http://library.divinity.duke.edu/"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'Divinity'});"
                            >
                              Divinity School Library
                            </a>
                          </li>
                          <li>
                            <a
                              href="http://library.fuqua.duke.edu/"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'Ford'});"
                            >
                              Ford Library, Fuqua School of Business
                            </a>
                          </li>
                          <li>
                            <a
                              href="http://law.duke.edu/lib/"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'Goodson Law'});"
                            >
                              Goodson Law Library
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://mclibrary.duke.edu/"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'Med Center'});"
                            >
                              Medical Center Library
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3 dul-masthead-featureColumn">
                        <a
                          href="https://library.duke.edu/rubenstein/"
                          className="dul-masthead-featureLink"
                          onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'About Menu', eventLabel: 'Feature'});"
                        >
                          <img
                            className="dul-masthead-megaFeature img-polaroid"
                            src="https://library.duke.edu/masthead/img/promos/libraries.jpg"
                            alt=""
                          />
                          <br />
                          <div className="dul-masthead-featureCaption">
                            Rubenstein Library
                          </div>
                        </a>
                      </div>
                      <div className="dul-masthead-col_12 dul-masthead-auxLinks">
                        <div className="dul-masthead-col_2 dul-masthead-moreLink">
                          <a
                            href="https://library.duke.edu/libraries"
                            aria-label="More about the libraries at Duke"
                            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'Libraries Menu', eventLabel: 'More'});"
                          >
                            More »
                          </a>
                        </div>
                        <div className="dul-masthead-col_10 dul-masthead-auxHelp" />
                      </div>
                    </div>
                  </div>
                  {/* End Item Container */}
                </li>
                {/* End Item */}
                <li className="dul-masthead-mega_toplevel">
                  <a
                    href="https://library.duke.edu/about"
                    onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'About Menu', eventLabel: 'About Portal'});"
                  >
                    About
                  </a>
                  {/* Begin Item */}
                  <div
                    className="dul-masthead-dropdown_fullwidth"
                    style={{ maxWidth: "none", width: 958, left: 0, top: "auto" }}
                  >
                    {/* Begin Item Container */}
                    <div className="dul-masthead-menu-items">
                      <div className="dul-masthead-col_3">
                        <ul>
                          <li>
                            <a
                              href="https://library.duke.edu/about/hours"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'About Menu', eventLabel: 'Hours'});"
                            >
                              Hours
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://directory.library.duke.edu"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'About Menu', eventLabel: 'Staff'});"
                            >
                              Staff &amp; Departments
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/about/jobs"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'About Menu', eventLabel: 'Jobs'});"
                            >
                              Jobs
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3">
                        <ul>
                          <li>
                            <a
                              href="https://library.duke.edu/about/directions"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'About Menu', eventLabel: 'Directions'});"
                            >
                              Directions, Maps, Parking
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/about/contact"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'About Menu', eventLabel: 'Contact'});"
                            >
                              Contact Us
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3">
                        <ul>
                          <li>
                            <a
                              href="https://blogs.library.duke.edu/"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'About Menu', eventLabel: 'News'});"
                            >
                              News &amp; Events
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/exhibits"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'About Menu', eventLabel: 'Exhibits'});"
                            >
                              Exhibits
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/about/reports-quickfacts"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'About Menu', eventLabel: 'Surveys Reports'});"
                            >
                              Reports &amp; Quick Facts
                            </a>
                          </li>
                          <li>
                            <a
                              href="https://library.duke.edu/support"
                              onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'About Menu', eventLabel: 'Support the Libraries'});"
                            >
                              Support the Libraries
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="dul-masthead-col_3 dul-masthead-featureColumn">
                        <a
                          href="http://blogs.library.duke.edu/magazine/"
                          className="dul-masthead-featureLink"
                          onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'About Menu', eventLabel: 'Feature'});"
                        >
                          <img
                            className="dul-masthead-megaFeature img-polaroid"
                            src="https://library.duke.edu/masthead/img/promos/about.jpg"
                            alt=""
                          />
                          <br />
                          <div className="dul-masthead-featureCaption">
                            Duke Libraries Magazine
                          </div>
                        </a>
                      </div>
                      <div className="dul-masthead-col_12 dul-masthead-auxLinks">
                        <div className="dul-masthead-col_2 dul-masthead-moreLink">
                          <a
                            href="https://library.duke.edu/about"
                            aria-label="More about Duke University Libraries"
                            onclick="ga('send', 'event', { eventCategory: 'Masthead', eventAction: 'About Menu', eventLabel: 'More'});"
                          >
                            More »
                          </a>
                        </div>
                        <div className="dul-masthead-col_10 dul-masthead-auxHelp" />
                      </div>
                    </div>
                  </div>
                  {/* End Item Container */}
                </li>
                {/* End Item */}
              </ul>
              {/* End Mega Menu */}
            </div>
            {/* End Menu Container */}
          </nav>
        </div>
      </header>
    </div>
  );
}

export default Masthead;
