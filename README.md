# DUL React Masthead Test

## Requirements
Node 14.0.0 or later

## Install for Local Development
- `git clone` project
- `cd` into project root
- `npm install`
- `npm start`
- The app is now available at [http://localhost:3000](http://localhost:3000) in your browser (and will reload when changes are made to code)

## A few notes...
- `./src/scriptParameters.json` is a stand-in for the parameters that will eventually be passed in via a `<script>` tag (or other loading method).
- `./src/index.js` renders the final markup to the DOM (where it's mounted in `#dul-masthead-root`).
- Data (aka 'props') flow down through nested Components, e.g. `App.js` --> `Masthead.js` --> ??? (maybe the dropdown tabs, search box, or other pieces are refactored into re-usable React Componenents?).
- React uses a syntax called JSX (Javascript XML) which is similar to a templating language in that it allows you to embed variables and expressions in your markup. I haven't done this yet here. Currently, the markup is hardcoded in `Masthead.js` for a fixed masthead...but the next step is to use the values from `scriptParameters' to calculate some additional variables and then insert those into the markup as needed.
- Similarly to above, Masthead content data (e.g. link titles/URLs, image paths, etc.) could be set via an easily editable JSON file and passed down to the Masthead and sub-components via App "state" and "props".

### To Do
- Convert any essential logic from `./src/load-masthead-legacy.js` to methods within the App or Masthead components (basically, add some methods for showing/hiding, etc.).
- Event handlers work a bit differently in React. `onclick` and `onsubmit` events need to be refactored in the proper syntax. A lot of this might be GA stuff that can be stripped out.
- Break out sub-Components (basically any piece that's repeated more than twice either within or across dropdown tabs) and refactor.
- Instead of trying to port everything over from the old app, take a step back and determine what's no longer needed. For example, we might not need all of those initial parameters any more, and we don't need three different style sheets. My sense is that overhauling the CSS will make a lot of the legacy code obsolete.

### Embed the Masthead in a Web page
- build the app for production: `npm run build`
- make sure the build files are hosted somewhere that will be available to the web page loading them
- add references to bundled/minified JS and CSS to the page head: `<script defer="defer" src="./static/js/main.b90a7529.js"></script><link href="./static/css/main.21d9eb69.css" rel="stylesheet">`
- add the placeholder div to the web page wherever you want the masthead: `<div id="dul-masthead-root"></div>`

### Resources
- [create-react-app](https://github.com/facebook/create-react-app)
- [React demo app (for reference / memory-jogging)](https://github.com/zinc-glaze/memory-matrix)
- [React docs](https://reactjs.org/docs/getting-started.html)
- [How to Embed React App in Website](https://betterprogramming.pub/how-to-embed-a-react-application-on-any-website-1bee1d15617f)
- [React library for Matomo](https://www.npmjs.com/package/@datapunt/matomo-tracker-react)
- [Deployment](https://create-react-app.dev/docs/deployment/)

---

Below is the create-react-app boilerplate:

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
